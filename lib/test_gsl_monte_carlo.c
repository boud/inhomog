/*
   inhomog - tools and main program for backreaction calculations

   Copyright (C) 2012 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <gsl/gsl_rng.h>
/* #include <gsl/gsl_math.h> */
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <math.h>

#include "lib/inhomog.h"

/* prototypes */
double our_function(double * x_array, size_t dim, void * params);
#define DIM_OUR_FN 3
double our_function_answer(void);


double our_function(double * x_array, size_t dim, void * params){
  /*  return 1.0; */ /* to start with :P */
  int dummy;
  void *dummy_pointer;
  dummy = (int)dim;
  dummy_pointer = params;
  (void)dummy; /* do something with dummy */
  (void)dummy_pointer; /* do something with dummy_pointer */
  return (x_array[0]*x_array[0]);
}

double our_function_answer(void){
  /*  correct answer to our_function */
  return (double)(1.0/3.0);
}




/* int test_gsl_monte_carlo(int n_calls )*/
  /* number of times to evaluate the function */
int main(void)
{
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  int n_calls = TEST_GSL_N_CALLS;

  int pass = 0; /* default test status = OK = 0 */


  gsl_monte_plain_state * working_space;

  /* a meta-function as per  gsl_monte_function_struct  in gsl_monte.h */
  gsl_monte_function our_meta_function;

  double x_lower[DIM_OUR_FN];
  double x_upper[DIM_OUR_FN];
  int i;

#define N_MEAN 30
  double the_integral;
  double integ_error;
  double the_integral_mean=0.0;
  double true_error_mean=0.0;
  double true_error_rms=0.0;
  int  i_integration;

  double true_error;

  /*  int debug=0; */

  gsl_rng_env_setup();
  /*  T_gsl = gsl_rng_default; */
  T_gsl = gsl_rng_mt19937; /* = gsl_rng_default */
  T_gsl = gsl_rng_ranlxd2; /* L"uscher's RANLUX algorithm, mathematically best, but 10x slower */
  gsl_rng_default_seed += 1997;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /* meta set up our function (defined in detail above) */
  our_meta_function.f = &our_function;
  our_meta_function.dim = DIM_OUR_FN;
  our_meta_function.params = NULL;  /* be safe */

  /* gsl_monte_plain_state * gsl_monte_plain_alloc (size_t DIM) */
  working_space =  gsl_monte_plain_alloc (DIM_OUR_FN);

  /*  int gsl_monte_plain_integrate (gsl_monte_function * F,
      const double XL[], const double XU[], size_t DIM, size_t
      CALLS, gsl_rng * R, gsl_monte_plain_state * S, double *
      RESULT, double * ABSERR) */

  /* define the cartesian region of integration */
  for (i=0; i<DIM_OUR_FN; i++){
    x_lower[i] = 0.0;
    x_upper[i] = 1.0;
  };

  for(i_integration=0;
      i_integration<N_MEAN;
      i_integration++){
    gsl_monte_plain_integrate ( &our_meta_function,
                                x_lower,
                                x_upper,
                                DIM_OUR_FN,
                                (size_t)n_calls,
                                r_gsl,
                                working_space,
                                &the_integral,
                                &integ_error);
    the_integral_mean += the_integral;
    true_error = the_integral - our_function_answer();
    true_error_mean += true_error;
    true_error_rms += true_error*true_error;
  }; /*   for(i_integration=0;  */

  if(N_MEAN>1){
    the_integral_mean /= (double)N_MEAN;
    true_error_mean /= (double)N_MEAN;
    true_error_rms = sqrt(true_error_rms / (double)N_MEAN );
  };

  printf("\ntest_gsl_monte_carlo:  the_integral = %f, last claimed err = %8e, mean err = %8e, rms err = %8e\n",
         the_integral_mean, integ_error, true_error_mean, true_error_rms);

  /* 3 sigma error in rms error estimate? */
  if(true_error_rms > 3.0 * integ_error){
    pass = 1;
    printf("rms err is greater than 3.0 times the last claimed err.\n");
  };

  if(!isfinite(true_error_rms)){
    pass += 2;
    printf("true_error_rms = %8e is not a valid number.\n",true_error_rms);
  };

  gsl_rng_free(r_gsl);
  gsl_monte_plain_free (working_space);

  printf("pass = %d\n",pass);
  return pass;
}
