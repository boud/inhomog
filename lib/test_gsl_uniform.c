/*
   inhomog - tools and main program for backreaction calculations

   Copyright (C) 2012 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <gsl/gsl_rng.h>
#include <math.h>

#include "lib/inhomog.h"


int test_gsl_uniform(int n_sigma  /* tolerable number of stand devs for tests */
                     );

int test_gsl_uniform(int n_sigma){
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  double overdensity[TEST_GSL_MAX_N];
  int i,i_bin;
  int return_value=0;
  double n_per_bin[TEST_GSL_N_BINS];
  double delta_overdensity;
  double mean_per_bin;
  double sig_per_bin; /* theoretical st.dev. per bin */
  double max_deviation = 0.0; /* actual max abs deviation in generated data */

  int debug=0;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 9473;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  for(i=0; i<TEST_GSL_MAX_N; i++){
    overdensity[i] = 2.0 * gsl_rng_uniform(r_gsl) -1.0; /* -1 to +1 */
  };

  for(i_bin=0; i_bin<TEST_GSL_N_BINS; i_bin++) n_per_bin[i_bin] = 0;
  delta_overdensity = (1.0 -(-1.0)) / (double)TEST_GSL_N_BINS;
  for(i=0; i<TEST_GSL_MAX_N; i++){

    i_bin = (int)(floor((overdensity[i] -(-1.0))/delta_overdensity) +0.01);
    if(debug && i<10){
      printf("test_gsl_uniform:  overdensity[i] = %f\n",
             overdensity[i]);
      printf("test_gsl_uniform:  i_bin = %d\n", i_bin);
    };
    if(i_bin >= 0 && i_bin < TEST_GSL_N_BINS){
      n_per_bin[i_bin] += 1.0;
    };
  };

  /* theoretical statistics */
  mean_per_bin = (double)TEST_GSL_MAX_N/(double)TEST_GSL_N_BINS;
  sig_per_bin = sqrt(mean_per_bin); /* in general, check sqrt( ) */

  /* is the maximum abs deviation per bin within n_sigma of what's
     expected ? */
  for(i_bin=0; i_bin<TEST_GSL_N_BINS; i_bin++){
    if(debug && i_bin < 200)
      printf("test_gsl_uniform: i_bin, n_per_bin[i_bin] = %d  %f\n",
             i_bin, n_per_bin[i_bin]);

    if(fabs(n_per_bin[i_bin] - mean_per_bin) > max_deviation){
      max_deviation = fabs(n_per_bin[i_bin] - mean_per_bin);
    };
    if(debug && i_bin < 200)
      printf("test_gsl_uniform: max_deviation = %f, theoretical mean_per_bin = %f\n",
             max_deviation, mean_per_bin);
  };

  /* TODO? add to TESTS_FAST list in Makefile.am */
  if( max_deviation <= (double)n_sigma * sig_per_bin){
    printf("test_gsl_uniform:  success; max_deviation = %f <= %d sigma = %f\n",
           max_deviation, n_sigma, (double)n_sigma * sig_per_bin);
  }else{
    printf("test_gsl_uniform:  fail; max_deviation = %f > %d sigma = %f\n",
           max_deviation, n_sigma, (double)n_sigma * sig_per_bin);
    return_value = 1;
  };

  gsl_rng_free(r_gsl);

  return return_value;
}

int main(void){
  int pass;
  pass = test_gsl_uniform(TEST_GSL_N_SIGMA);  /* tolerable number of stand devs for tests */
  printf("pass = %d\n",pass);
  return pass;
}
