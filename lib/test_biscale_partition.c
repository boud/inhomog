/*
   test_biscale_partition - test biscale_partition

   Copyright (C) 2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include "lib/inhomog.h"
#include "lib/biscale_partition.h"

/* This is a code regression unit test. It assumes that parameters
   in biscale_partition.c (e.g. a_FLRW_init, H1bg, N_TIMES, DELTA_COLLAPSE)
   have not been changed. It also checks for computational accuracy,
   consistency of arithmetic, etc. on different operating systems.
*/

int main(void){

  enum collapse_types collapse_type[N_COLLAPSE_TYPES] =
    { planar_collapse,
      spherical_expansion,
      III_zero,
      II_zero };

  int i_collapse;

  /* Collapse times in Gyr - the Newtonian fluid T^3 cosmology model
     holds prior to these times. */
  double t_collapse[N_COLLAPSE_TYPES] =
    { 6.1, 4.3, 3.0, 3.5
    };
  /* max(abs(aEdS-aeff)/aEdS) */
  double aeff_aEdS_on_aEdS[N_COLLAPSE_TYPES] =
    { 3e-5, 0.02, 0.04, 0.03
    };

  double t_background[N_TIMES];
  double a_FLRW[N_TIMES];
  double a_global_QZA[N_TIMES];
  double a_D[N_TWO][N_TIMES]; /* a_D local QZA */
  double a_D_Newt[N_TWO][N_TIMES]; /* a_D local Newt */

  int32_t i_t;

  int32_t pass = 0;
  int32_t failed_this_time;
  int32_t failflag = 1;
  int want_verbose = 0;

  double I_inv[N_TWO];
  double II_inv[N_TWO];
  double III_inv[N_TWO];


  /* Expanding domain initial invariants; some may be overwritten. */
  I_inv[0] = 0.01;
  II_inv[0] = 1e-4;
  III_inv[0] = 1e-6;

  printf("\n"); /* newline to conform with pattern of other test routines */

  for (i_collapse=0; i_collapse<N_COLLAPSE_TYPES; i_collapse++){

    biscale_partition(collapse_type[i_collapse],
                      want_verbose,
                      /* initial invariants, values */
                      I_inv,
                      II_inv,
                      III_inv,
                      t_background,
                      a_FLRW,
                      a_global_QZA,
                      a_D, /* TODO: add a test for this? */
                      a_D_Newt /* TODO: add a test for this? */
                      );
    /* Check if any pre-virialisation ratios of a_eff to a_EdS
       diverge from unity by more than they should; add a fail
       flag if that occurs. */
    failed_this_time = 0;
    for(i_t=0;
        i_t<N_TIMES && t_background[i_t] < t_collapse[i_collapse]
          && (!failed_this_time);
        i_t++){
      if( !isfinite(fabs((a_global_QZA[i_t] - a_FLRW[i_t])/a_FLRW[i_t])) ||
          fabs((a_global_QZA[i_t] - a_FLRW[i_t])/a_FLRW[i_t]) >
          aeff_aEdS_on_aEdS[i_collapse] ){
        pass += failflag;
        failed_this_time = 1;
        printf("test_biscale_partition fail: %g %g %g %g.\n",
               t_background[i_t], aeff_aEdS_on_aEdS[i_collapse],
               a_FLRW[i_t], a_global_QZA[i_t]);
      } else {
        /* printf("test_biscale_partition OK: %g %g %g %g.\n",
               t_background[i_t], aeff_aEdS_on_aEdS[i_collapse],
               a_FLRW[i_t], a_global_QZA[i_t]); */
      };
    };

    failflag *= 2;
  };

  printf("pass = %d\n",pass);

  return pass;
}
