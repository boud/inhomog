/* 
   test GSL ODE integrator header file

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski 

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

int test_inhomog_ODE_func(double t,
                          const double func_first_order[],
                          double func_first_order_deriv[],
                          void *params);

int test_inhomog_ODE_jacob(double t,
                           const double func_first_order[],
                           double *dfunc1D_partials,
                           double dfunc1D_dt[],
                           void *params);


