/*
   biscale_partition - illustrate VQZA with T3 = two domains

   Copyright (C) 2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file biscale_partition.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"
#include "lib/alloc_big_array.h"
#include "lib/biscale_partition.h"

/*! \brief If called, sets main parameters and matrices for further use.
 *
 * The scale factor matrices and time values matrix size is specified
 * by \b N_TIMES and defined in biscale_partition.h.
 *
 * Calculates invariants based on the collapse type (planar collapse or
 * spherical expansion). Invariants II and III may be also manually set
 * to 0 by setting the collapse type to II_zero or III_zero.
 *
 * background_cosm_params_s structure elements must be set before the
 * main integration.
 *
 * Main part of the function calls \ref Omega_D and
 * \ref find_collapse_time functions.
 *
 * If \a want_verbose is set to 1, prints out parameter values at the
 * end.
 *
 * \param [in] collapse types enumerates the possible collapse type
 * \param [in] want_verbose parameter controlling the amount of
 * commentary printed out during compilation
 * \param [out] t_background_1cosm matrix of time values
 * \param [out] a_FLRW [TODO]
 * \param [out] a_global_QZA global QZA scale factor
 * \param [out] a_D local QZA scale factor
 * \param [out] a_D_Newt local Newtonian scale factor
 */
int biscale_partition(/* INPUTS */
                      enum collapse_types collapse_type,
                      int want_verbose,
                      /* INPUTS/OUTPUTS: initial invariants, values */
                      double I_inv[N_TWO],
                      double II_inv[N_TWO],
                      double III_inv[N_TWO],
                      /* OUTPUTS */
                      double t_background_1cosm[N_TIMES],
                      double a_FLRW[N_TIMES],
                      double a_global_QZA[N_TIMES],
                      double a_D[N_TWO][N_TIMES], /* a_D local QZA */
                      double a_D_Newt[N_TWO][N_TIMES] /* a_D local Newt */
                      ){

  /* use the i_EdS case only */
#define N_COSM_I 1
#define N_COSM 2

#define DELTA_COLLAPSE (18.0*M_PI*M_PI)

  int32_t n_domains = N_TWO;
  int32_t n_t_inhomog = N_TIMES;
  int32_t i_domain;

  /* initial scale factor and H */
  const double a_FLRW_init = 0.005;
  const double H1bg = 37.7;
  double t_i[N_COSM];
  double t_0[N_COSM]; /* Universe age */
  double delta_t; /* time interval for outputting integrated values */
  int32_t i_t;
  double t_background[N_COSM][N_TIMES];

  /* per-domain values */
  double rza_Q_D[N_TWO][N_TIMES]; /* Q local */
  double rza_R_D[N_TWO][N_TIMES]; /* R local */
  double V_D[N_TWO][N_TIMES]; /* V_D local * N_TWO*/ /* not normalised: sum(V_D) = 2 */
  double dot_a_D[N_TWO][N_TIMES]; /* a_D local */
  double lambda[N_TWO][N_TIMES]; /* volume fractions */
  double H_D[N_TWO][N_TIMES]; /* expansion rate */
  int i_t_collapse[N_TWO];
  int collapsed[N_TWO];

  /* per-domain values where the underdense domain is the exact EdS
     complement of the overdense domain */
  double V_D_Newt[N_TWO][N_TIMES];   /* not normalised: sum(V_D_Newt) = 2 */
  double lambda_Newt[N_TWO][N_TIMES];
  double H_D_Newt[N_TWO][N_TIMES];
  double OmR_D_Newt[N_TWO][N_TIMES];

  /* global values */
  double vol_sum[N_TIMES]; /* total volume; normalised at a late step in the calculation */
  double Q_glob_WB10_21[N_TIMES]; /* global Q from WiegBuch2010 [arXiv:1002.3912] Eq. (21) */
  double H_glob[N_TIMES]; /* global H */
  double OmR_glob[N_TIMES]; /* global OmR */

  struct rza_integrand_params_s rza_integrand_params;
  struct background_cosm_params_s background_cosm_params;

  int i_EdS = 1;
  int want_planar = 0;
  int want_spherical = 0;

  /* many of the following are calculated but not used */
  long   n_calls_invariants = 100; /* dummy */
  double n_sigma[3] = {0.0, 0.0, 0.0}; /* dummy */
  int unphysical[N_TWO][N_TIMES];
  double Omm_D[N_TWO][N_TIMES];
  double OmQ_D[N_TWO][N_TIMES];
  double OmLam_D[N_TWO][N_TIMES];
  double OmR_D[N_TWO][N_TIMES];
  double OmR_D_globnorm[N_TWO][N_TIMES]; /* normalised by global H */

  /* ********** biscale partition initial invariants *********/
  /*
  I_inv[0] = 0.01;
  II_inv[0] = 1e-4;
  III_inv[0] = 1e-6;
  */

  /*
  I_inv[0] = 0.1;
  II_inv[0] = 0.01;
  III_inv[0] = 0.001;
  */

  switch (collapse_type) {
  case planar_collapse:
    II_inv[0] = 0.0;
    III_inv[0] = 0.0;
    break;
  case spherical_expansion:
    II_inv[0] = I_inv[0]*I_inv[0]/3.0;
    III_inv[0] = I_inv[0]*I_inv[0]*I_inv[0]/27.0;
    break;
  case III_zero:
    III_inv[0] = 0.0;
    break;
  case II_zero:
    II_inv[0] = 0.0;
    break;
  default:
    break;
  };


  /* expressible as divergences => apply Stokes' theorem */
  I_inv[1] = -I_inv[0];
  II_inv[1] = -II_inv[0];
  III_inv[1] = -III_inv[0];


  /* ********** set up integration and cosmological parameters ************/


  /* These parameters should have no effect: */
  rza_integrand_params.w_type = 1; /* test standard spherical domains */
  rza_integrand_params.delta_R = DELTA_R_DEFAULT; /*  if(2==rza_integrand_params.w_type) */
  /* 'B' = BBKS (BKS version); 'e' = EisHu98 short formula set ;
     'E' = EisHu98 full formula set */
  rza_integrand_params.pow_spec_type = 'E';

  /* This parameter *must* have an effect: */
  rza_integrand_params.precalculated_invariants.enabled = 1;

  if(want_verbose)printf("biscale_partition: number of subdomains: %d\n",n_domains);

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = a_FLRW_init;
  background_cosm_params.inhomog_a_d_scale_factor_initial = a_FLRW_init;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;


  background_cosm_params.EdS = i_EdS;
  if(want_verbose)printf("background_cosm_params.EdS  = %d\n",
                         background_cosm_params.EdS);

  background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */
  background_cosm_params.Theta2p7 = 2.726/2.7; /* e.g. arXiv:0911.1955 */

  background_cosm_params.H_0 = H1bg;
  background_cosm_params.Omm_0 = 1.0;
  background_cosm_params.OmLam_0 = 0.0;
  background_cosm_params.Ombary_0 = 0.022
    /exp(2.0*log(background_cosm_params.H_0 / 100));
  /* Planck: Ade et al 2013 arXiv:1303.5076v3, Table 2 */
  background_cosm_params.sigma8 = 0.83;

  t_i[i_EdS] = t_EdS(&background_cosm_params,
                     background_cosm_params.inhomog_a_scale_factor_initial,
                     want_verbose);

  background_cosm_params.recalculate_t_0 = 1;

  if(want_verbose)printf("\nbiscale_partition:\n");

  t_0[i_EdS] = background_cosm_params.t_0;
  delta_t = (t_0[i_EdS]-t_i[i_EdS])/((double)(n_t_inhomog-1));

  for(i_t=0; i_t<n_t_inhomog; i_t++){
    /* possible TODO: do a loop over i_EdS = 0 [LCDM = not EdS], 1 [EdS] */
    t_background[i_EdS][i_t] = t_i[i_EdS] + (double)i_t * delta_t;
    t_background_1cosm[i_t] = t_background[i_EdS][i_t];
  };


  /* ***** do the main integration ******/
  for(i_domain=0; i_domain<n_domains; i_domain++){

    if(want_verbose) printf("I,II,III = %g %g %g\n",
                            I_inv[i_domain], II_inv[i_domain], III_inv[i_domain]);

    rza_integrand_params.precalculated_invariants.inv_I = I_inv[i_domain];
    rza_integrand_params.precalculated_invariants.inv_II = II_inv[i_domain];
    rza_integrand_params.precalculated_invariants.inv_III = III_inv[i_domain];

    Omega_D(&rza_integrand_params,
            background_cosm_params,
            (double*)&(t_background[i_EdS][0]), n_t_inhomog,
            n_sigma,
            n_calls_invariants,
            want_planar, /* cf RZA2 V.A */
            want_spherical,
            want_verbose,
            (double*)&(rza_Q_D[i_domain][0]),
            (double*)&(rza_R_D[i_domain][0]),
            (double*)&(a_D[i_domain][0]),
            (double*)&(dot_a_D[i_domain][0]),
            (int*)&(unphysical[i_domain][0]),
            (double*)&(H_D[i_domain][0]),
            (double*)&(Omm_D[i_domain][0]),
            (double*)&(OmQ_D[i_domain][0]),
            (double*)&(OmLam_D[i_domain][0]),
            (double*)&(OmR_D[i_domain][0])
            );

    find_collapse_time(n_t_inhomog,
                       (double*)&(a_D[i_domain][0]),
                       (double*)&(dot_a_D[i_domain][0]),
                       (int*)&(i_t_collapse[i_domain]),
                       (int*)&(collapsed[i_domain]));
  };  /*    for(i_domain=0; i_domain<n_domains; i_domain++) */

  if(want_verbose)  printf("collapse / i_t_collapse = %d / %d, %d / %d\n",
                           collapsed[0], i_t_collapse[0],
                           collapsed[1], i_t_collapse[1]);

  /*
    Scroll below to check that this hint line matches what is actually output.
  */

  if(want_verbose)
    printf("t aEdS : a_D a_D lambda lambda vol_tot aeff : a_D_Newt0 1 : Q Q H H Hglob : QglobWB10_21 : OmR OmR Newt Newt OmRsum\n");

  for(i_t=0; i_t<n_t_inhomog; i_t++){

    a_FLRW[i_t] =
      a_EdS(&background_cosm_params,
            t_background[i_EdS][i_t],
            want_verbose);

    vol_sum[i_t] = 0.0;
    for(i_domain=0; i_domain<n_domains; i_domain++){
      if(!collapsed[i_domain] ||
         (collapsed[i_domain] && i_t < i_t_collapse[i_domain]) ){
        V_D[i_domain][i_t] = exp(3.0* log(a_D[i_domain][i_t]));
      }else{
        V_D[i_domain][i_t] =
              pow( a_EdS(&background_cosm_params,
                         t_background[i_EdS][i_t_collapse[i_domain]],
                         want_verbose), 3.0 ) /
              DELTA_COLLAPSE;
        /* in this case, replace RZA scale factor by virialisation scale factor */
        a_D[i_domain][i_t] =
          pow( V_D[i_domain][i_t], 1.0/3.0 );
      };
      vol_sum[i_t] += V_D[i_domain][i_t];

      /* ASSUME that domain 0 is the expanding domain : TODO - protect against user modification error */
      V_D_Newt[1][i_t] = V_D[1][i_t]; /* overdense is unchanged by stable clustering hypothesis */
      /* factor of 2.0 here is because of non-normalisation of "V_D.*"
         - there are two domains */
      V_D_Newt[0][i_t] = (double)N_TWO * pow( a_FLRW[i_t], 3.0 ) - V_D_Newt[1][i_t];

    }; /*  for(i_domain=0; i_domain<n_domains; i_domain++) */

    /* Use vol_sum to normalise other quantities, before normalising vol_sum itself. */
    for(i_domain=0; i_domain<n_domains; i_domain++){
      lambda[i_domain][i_t] = V_D[i_domain][i_t] / vol_sum[i_t];

      a_D_Newt[i_domain][i_t] = pow(V_D_Newt[i_domain][i_t], 1.0/3.0);
      lambda_Newt[i_domain][i_t] = V_D_Newt[i_domain][i_t] / vol_sum[i_t];
    };

    /* Normalise vol_sum */
    vol_sum[i_t] /= (double)N_TWO;
    a_global_QZA[i_t] = exp(log(vol_sum[i_t])/3.0);




    if( (!collapsed[0] ||
         (collapsed[0] && i_t < i_t_collapse[0]) ) &&
        (!collapsed[1] ||
         (collapsed[1] && i_t < i_t_collapse[1]) ) ){

      Q_glob_WB10_21[i_t] =
        lambda[0][i_t] * rza_Q_D[0][i_t] +
        lambda[1][i_t] * rza_Q_D[1][i_t] +
        6.0 * lambda[0][i_t] * lambda[1][i_t] *
        (H_D[0][i_t] - H_D[1][i_t]) * (H_D[0][i_t] - H_D[1][i_t]);

      H_glob[i_t] =
        lambda[0][i_t] * H_D[0][i_t] +
        lambda[1][i_t] * H_D[1][i_t];
      for(i_domain=0; i_domain<n_domains; i_domain++){
        OmR_D_globnorm[i_domain][i_t] = OmR_D[i_domain][i_t] *H_D[i_domain][i_t]*H_D[i_domain][i_t] /
          (H_glob[i_t]*H_glob[i_t]);
      };
      /* ASSUME that domain 0 is the expanding domain : TODO - protect against user modification error */
      OmR_D_Newt[1][i_t] = OmR_D[1][i_t]; /* collapsing domain as per VQZA */
      H_D_Newt[1][i_t] = H_D[1][i_t]; /* collapsing domain as per VQZA */

      H_D_Newt[0][i_t] = (H_glob[i_t] - lambda_Newt[1][i_t]* H_D_Newt[1][i_t]) / lambda_Newt[0][i_t];
      OmR_D_Newt[0][i_t] =
        - lambda_Newt[1][i_t]/lambda_Newt[0][i_t] * OmR_D_globnorm[1][i_t]
        * H_glob[i_t]*H_glob[i_t] /(H_D_Newt[0][i_t]*H_D_Newt[0][i_t]);
      OmR_glob[i_t] =
        (lambda[0][i_t] * OmR_D_globnorm[0][i_t] +
         lambda[1][i_t] * OmR_D_globnorm[1][i_t] );

    } else if( collapsed[1] && i_t >= i_t_collapse[1]){
      Q_glob_WB10_21[i_t] = rza_Q_D[0][i_t]; /* approximate: lambda[0] = 1 && lambda[1] = 0 */
      H_glob[i_t] = H_D[0][i_t];

      OmR_D_globnorm[0][i_t] = OmR_D[0][i_t] *H_D[0][i_t]*H_D[0][i_t] /
        (H_glob[i_t]*H_glob[i_t]);
      OmR_D_globnorm[1][i_t] = 0.0; /* assume virialisation => zero curvature */

      /* stable clustering/Newtonian hypothesis */
      OmR_D_Newt[0][i_t] = 0.0;
      OmR_D_Newt[1][i_t] = 0.0;

      /* parameters of expanding domain dominate */
      OmR_glob[i_t] = OmR_D_globnorm[0][i_t];
    };



    if(want_verbose)
      printf("t: %g %20.16g : %g %g %g %g %g %20.16g : %g %g : %g %g %g %g %g : %g : %g %g %g %g %g\n",
       t_background[i_EdS][i_t],
       a_FLRW[i_t],
       /* : */
       a_D[0][i_t], a_D[1][i_t],
       lambda[0][i_t], lambda[1][i_t],
       vol_sum[i_t], a_global_QZA[i_t],
       /* : */
       a_D_Newt[0][i_t], a_D_Newt[1][i_t],
       /* : */
       rza_Q_D[0][i_t], rza_Q_D[1][i_t],
       H_D[0][i_t], H_D[1][i_t],
       H_glob[i_t],
       /* : */
       Q_glob_WB10_21[i_t],
       /* : */
       OmR_D[0][i_t],OmR_D[1][i_t],
       OmR_D_Newt[0][i_t],OmR_D_Newt[1][i_t],
       OmR_glob[i_t]
    );
  }; /*   for(i_t=0; i_t<n_t_inhomog; i_t++) */

  return 0;
}
