/*************************************************************************
 *   c_gsl_wrap - C front ends for bits of GNU Scientific Library        *
 *                                                                       *
 *   Copyright (C) 2016, 2017 by Boud Roukema                                  *
 *   boud cosmo.torun.pl                                                 *
 *                                                                       *
 *   This program is free software; you can redistribute it and/or modify*
 *   it under the terms of the GNU General Public License as published by*
 *   the Free Software Foundation; either version 2 of the License, or   *
 *   (at your option) any later version.                                 *
 *                                                                       *
 *   This program is distributed in the hope that it will be useful,     *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
 *   GNU General Public License for more details.                        *
 *                                                                       *
 *   You should have received a copy of the GNU General Public License   *
 *   along with this program; if not, write to the                       *
 *   Free Software Foundation, Inc.,                                     *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA            *
 *   or see https://www.gnu.org/licenses/licenses.html#GPL .             *
 *************************************************************************/

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_qrng.h>
#ifdef __GNUC__
#include <stdio.h>
#include <strings.h>
#endif

#define N_SIGMA_GAUSS 1.0

void wrap_gsl_rng_uniform(long iseed,
                          int  i_free, /* 0 = initialise, 1 = free the memory */
                          double *ran_double);


void wrap_gsl_qrng_3D(int  i_free, /* 0 = initialise, 1 = free the memory */
                      double ran_double_3D[3]);

void gauss_gsl(long Seed,
               double *RandNum,
               int free_gauss_gsl);

/* random double precision float generator

   Especially written for convenient calls
   from fortran90, requiring only a single fortran-side
   wrapper for initialising, calling, and freeing.
 */

void wrap_gsl_rng_uniform(long iseed,
                          int  i_free, /* 0 = initialise, 1 = free the memory */
                          double *ran_double)
{
  static const gsl_rng_type * T_gsl;
  static gsl_rng * r_gsl;
  static int have_initialised = 0;

  /* MT19937 - Makoto Matsumoto and Takuji Nishimura;
     Mersenne prime period of 2^19937 - 1;
     second revision 2002 - info gsl for details */
  /* T_gsl = gsl_rng_mt19937; */

  /* RANLUX algorithm of L\"uscher;
     period \sim 10^171;
     second generation algorithm;
     double-precision, highest luxury level;
     slow: about 7-10 times slower than MT19937;
     claims of well-tested randomness - info gsl for more
  */
  T_gsl = gsl_rng_ranlxd2;

  if(!have_initialised){
    gsl_rng_default_seed = (unsigned long) iseed;
    r_gsl = gsl_rng_alloc(T_gsl);
    have_initialised = 1;
  };

  if(!i_free){
    *ran_double = gsl_rng_uniform(r_gsl);
  }else if(have_initialised){
    gsl_rng_free(r_gsl);
    have_initialised = 0;
  };
}

void wrap_gsl_qrng_3D(int  i_free, /* 0 = initialise, 1 = free the memory */
                      double ran_double_3D[3])
{
  static gsl_qrng * r_gsl;
  static int have_initialised = 0;

  if(!have_initialised){
    r_gsl = gsl_qrng_alloc(gsl_qrng_niederreiter_2, 3);
    have_initialised = 1;
  };

  if(!i_free){
    gsl_qrng_get(r_gsl, ran_double_3D);
  }else if(have_initialised){
    gsl_qrng_free(r_gsl);
    have_initialised = 0;
  };
}

/* Independent mpi threads should create independent instances of this
   sequence; providing different Seed's will give different sequences. */
void gauss_gsl(long Seed,
               double *RandNum,
               int free_gauss_gsl){

  /* The static attribute should work for mpi (independent memory per
     thread). If porting to openmp (shared memory) is needed, test this
     before trusting it.
  */
  static int first_time = 1;
  static const gsl_rng_type * T_gsl;
  static gsl_rng * r_gsl = NULL;

  static const double n_sigma_gauss = N_SIGMA_GAUSS;

  if(!first_time && free_gauss_gsl){
    gsl_rng_free(r_gsl);
    first_time = 1; /* in case this function gets called again after
                       freeing*/
  }else{

    if(first_time){
       first_time = 0; /* no longer first time */
      /* initialise the random number generator */

      T_gsl = gsl_rng_ranlxd2;
      /* A faster option could be:
         T_gsl = gsl_rng_mt19937;
         See the GSL documentation for more generators.
      */
      gsl_rng_default_seed = (unsigned long) Seed;
      r_gsl = gsl_rng_alloc(T_gsl); /* uses gsl_rng_default_seed */
    };

    /* generate double-precision gaussian random value
       https://en.wikipedia.org/wiki/Ziggurat_algorithm
     */
    *RandNum = gsl_ran_gaussian_ziggurat(r_gsl,
                                         n_sigma_gauss);

  };
}
