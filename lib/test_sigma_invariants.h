/* 
   test_sigma_invariants - brute force test of BKS 2000 Appendix C formulae

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

#define TEST_INVAR_kpeak 0.123
/* #define TEST_INVAR_kmax 2.46 */
/* 2pi/0.123 = 51.083 */
#define TEST_INVAR_R_domain 150.0
#define TEST_INVAR_kmax 1.0
#define TEST_INVAR_kmax_fraction_vary 0.50
#define TEST_INVAR_Pk_norm 1.0
#define TEST_INVAR_n_HarrZel 1.0
/* for generating A_k, phi_k */
/* #define TEST_INVAR_N_k 2000 */
/* #define TEST_INVAR_N_k 16 */
#define TEST_INVAR_N_k 2
/* cutoff near-zero amplitudes so that they're above zero */
#define TEST_INVAR_Ak_min 1e-200 

#define TEST_INVAR_N_realisations 100
/* n_calls for GSL Monte Carlo integration */

/* fast */
/* #define TEST_INVAR_GSL_RNG_TYPE gsl_rng_mt19937 */
/* best, slower by factor of 10 */
#define TEST_INVAR_GSL_RNG_TYPE gsl_rng_ranlxd2 

#define TEST_INVAR_TEST1D 1

#undef TEST_INVAR_TEST1D


/* for test_fftw3 */
#define TEST_INVAR_N_x 128


double power_spec3D(/* INPUTS */
                    double k_signed
                    /* OUTPUTS */
                    );

long index3D(int i,
             int j,
             int k,
             int N);


struct delta_tilde_struct_s 
{
  gsl_complex * delta_tilde; /* [TEST_INVAR_N_k][TEST_INVAR_N_k][TEST_INVAR_N_k], */
  double *i_array;
  double *j_array;
  double *k_array;
  /*  double d_k; */
};


/* randomly generate an delta_tilde array from A_k and phi_k random
   values according to the power spectrum */
int get_delta_tilde_realisation(/* INPUTS */
                                unsigned long int rng_seed,
                                /* OUTPUTS */
                                double i_array[TEST_INVAR_N_k], 
                                double j_array[TEST_INVAR_N_k], 
                                double k_array[TEST_INVAR_N_k], 
                                gsl_complex *delta_tilde);
/*                              double *d_k; */

int check_delta_tilde1D(struct delta_tilde_struct_s * delta_tilde_struct);


double C7_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct
                      );

double C14_integral(struct delta_tilde_struct_s *delta_tilde_struct
                    );

double C8_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct
                      );

double C18_integral(struct delta_tilde_struct_s *delta_tilde_struct
                    );

double C9_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct
                      );

double C20_integral(struct delta_tilde_struct_s *delta_tilde_struct
                    );

 
