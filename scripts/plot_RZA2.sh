#!/bin/bash

echo "This test assumes that you have plotutils installed."

LANG=C
TMP=`mktemp /tmp/tmp.rza2.XXXXXXXX`

OMD_FILE=$1
FORMAT=ps

NAME=Fig2left
grep "nsig=111 i_EdS=1 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$16}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$16}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$16}' >> ${TMP}

graph -T${FORMAT} -X t -Y "a_D/a" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig2right
grep "nsig=111 i_EdS=0 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$16}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$16}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$16}' >> ${TMP}

graph -T${FORMAT} -X t -Y "a_D/a" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig3left
grep "nsig=111 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP}

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig3right
grep "nsig=111 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=111 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,(1.0-$21)}' >> ${TMP} ## 1 - Om_Lambda^D
echo "" >> ${TMP}
grep "nsig=111 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP}

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}


NAME=Fig4left
grep "nsig=222 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=1 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig4right
grep "nsig=222 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=222 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,(1.0-$21)}' >> ${TMP} ## 1 - Om_Lambda^D
echo "" >> ${TMP}
grep "nsig=222 i_EdS=0 .*D  0\.497512" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}


NAME=Fig5left
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig5right
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,(1.0-$21)}' >> ${TMP} ## 1 - Om_Lambda^D
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.248756" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}



NAME=Fig6left
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=1 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" -y -7 7 ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

NAME=Fig6right
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$19}' > ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$20}' >> ${TMP}
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,(1.0-$21)}' >> ${TMP} ## 1 - Om_Lambda^D
echo "" >> ${TMP}
grep "nsig=\-1\-1\-1 i_EdS=0 .*D  0\.124378" ${OMD_FILE} |awk '{print $14,$22}' >> ${TMP} 

graph -T${FORMAT} -X t -Y "Omega^D's" -L "${NAME}" -y -1.5 1.5 ${TMP} > /tmp/OmRZA2${NAME}.${FORMAT}

ls -lt ${TMP} /tmp/OmRZA2*


